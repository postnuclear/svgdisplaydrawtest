package com.example.svgdisplaydrawtest;

import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;

import android.support.v7.app.ActionBarActivity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import android.view.View.OnTouchListener;

import android.graphics.Color;

import android.util.Log;


public class SVGDisplayDrawActivity extends ActionBarActivity 
{
	ImageView image;
	
	SVG svg;
	Drawable drawable;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		String TAG = "SVGDisplayDrawActivity.onCreate";
		
		setContentView(R.layout.activity_svgdisplay_draw);
		
		image = (ImageView) findViewById(R.id.imageView1);

		image.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		
		svg = SVGParser.getSVGFromResource(getResources(), R.raw.biuro);
        // Picture picture = svg.getPicture();
        drawable = svg.createPictureDrawable();		
		// Get a drawable from the parsed SVG and set it as the drawable for the ImageView
        image.setImageDrawable(drawable); 
        
        Log.i(TAG, "svg bounds: " + svg.getBounds());
        Log.i(TAG, "svg limits: " + svg.getLimits());
        Log.i(TAG, "drawable bounds: " + drawable.getBounds());
        
        image.setOnTouchListener(new ImageTouchListener());  
        
        //image.setDrawingCacheEnabled(true);        
        //image.buildDrawingCache(true);
     
        // creates immutable clone 
        //Bitmap bm = Bitmap.createBitmap(image.getDrawingCache(true)); 
        //Bitmap bm = image.getDrawingCache(true);
        
        //if (bm == null)
        //	Log.i(TAG, "Bitmap: null");

        //Log.i(TAG, "Bitmap: w=" + bm.getWidth() + " h=" + bm.getHeight());	
	}
	
	protected class ImageTouchListener implements OnTouchListener
	{
        public ImageTouchListener()
		{
		}
        
        public boolean onTouch (View v, MotionEvent event)
        {
        	String TAG = "OnTouchListener.onTouch";
        	
            Log.i(TAG, "event: " + event);           
            drawAndShow();
        	
        	return true;
        }		
	} // end of inner class
	
	
	protected void drawAndShow()
	{
    	String TAG = "drawAndShow";
    	
        Log.i(TAG, "svg bounds: " + svg.getBounds());
        Log.i(TAG, "svg limits: " + svg.getLimits());
        Log.i(TAG, "drawable bounds: " + drawable.getBounds());	
        
        Bitmap bm = image.getDrawingCache(true);
        
        if (bm == null)
        {
        	Log.i(TAG, "Bitmap: null");
        	return;
        }

        Log.i(TAG, "Bitmap: w=" + bm.getWidth() + " h=" + bm.getHeight());	
        
        Canvas cnv = new Canvas(bm);
        Paint linePaint = new Paint();
        linePaint.setColor(Color.RED);
        // linePaint.setStrokeWidth (4);
        cnv.drawLine(256, 256, 512, 512, linePaint);
        }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.svgdisplay_draw, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) 
		{
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}